﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrchardDiaryApp.Model
{
    public class TypeOfFruit
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Type { get; set; }
    }
}
