﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.Model
{
    public enum ETypeOfActivity
    {
        Hoeing, 
        Pruning, 
        Pesticide, 
        PesticideApplying,  
        Harvesting, 
        Inspection
    }
}
