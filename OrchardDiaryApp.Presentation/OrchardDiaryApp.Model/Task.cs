﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrchardDiaryApp.Model
{
    public class Task
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int TypeOfFruit { get; set; }
        [Required]
        public ETypeOfActivity TypeOfActivity { get; set; }
        [Required]
        public DateTime DateOfActivity { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public bool Deleted { get; set; }

        public Task() { }

        public Task(int eTypeOfFruit,ETypeOfActivity eTypeOfActivity,DateTime DateOfActivity,string Note,bool Deleted)
        {
            this.TypeOfFruit = eTypeOfFruit;
            this.TypeOfActivity = eTypeOfActivity;
            this.DateOfActivity = DateOfActivity;
            this.Note = Note;
            this.Deleted = Deleted;
        }
    }
}
