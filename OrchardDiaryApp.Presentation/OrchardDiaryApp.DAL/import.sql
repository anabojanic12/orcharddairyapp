CREATE DATABASE orcharddatabase;
USE orcharddatabase;

CREATE TABLE Fruit (
    Id INT PRIMARY KEY IDENTITY(1, 1),
    Type VARCHAR(255) NOT NULL,
);

INSERT INTO Fruit (Type) VALUES ('Apples and pears');
INSERT INTO Fruit (Type) VALUES ('Citrus');
INSERT INTO Fruit (Type) VALUES ('Stone fruit');
INSERT INTO Fruit (Type) VALUES ('Tropical and exotic ');
INSERT INTO Fruit (Type) VALUES ('Berries');
INSERT INTO Fruit (Type) VALUES ('Melons');
INSERT INTO Fruit (Type) VALUES ('Tomatoes and avocados');


CREATE TABLE Task(
 Id INT IDENTITY(1, 1) PRIMARY KEY,
 TypeOfActivity VARCHAR(255) NOT NULL,
 DateOfActivity DATE NOT NULL,
 Note VARCHAR(255) NOT NULL,
 Deleted BIT NOT NULL,
 TypeOfFruit INT FOREIGN KEY REFERENCES Fruit(Id)
)  

INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Hoeing','2020-12-02','task number one',0,1);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Pruning','2020-12-02','task number two',0,2);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('PesticideApplying','2020-12-02','task number three',0,3);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('PesticideApplying','2020-12-03','task number four',0,4);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Hoeing','2020-12-04','task number five',0,5);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Harvesting','2020-12-05','task number six',0,6);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Inspection','2020-12-06','task number seven',0,7);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Inspection','2020-12-07','task number eight',0,1);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Pesticide','2020-12-08','task number nine',0,2);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Harvesting','2020-12-09','task number text',0,3);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Harvesting','2020-12-10','task number eleven',0,4);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Pesticide','2020-12-11','task number twelve',0,5);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Pruning','2020-12-12','task number thirteen',0,6);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('Harvesting','2020-12-13','task number fourteen',0,6);
INSERT INTO Task(TypeOfActivity,DateOfActivity,Note,Deleted,TypeOfFruit) VALUES ('PesticideApplying','2020-12-14','task number fifteen',0,7);