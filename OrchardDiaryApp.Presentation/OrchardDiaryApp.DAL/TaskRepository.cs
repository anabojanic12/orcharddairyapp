﻿using OrchardDiaryApp.DAL.Contracts;
using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace OrchardDiaryApp.DAL
{
    public class TaskRepository : ITaskRepository
    {
        private string _connection;
        public TaskRepository(string connectionString)
        {
            this._connection = connectionString;
        }
        public void AddTask(Task task)
        {
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO Task(TypeOfActivity,TypeOfFruit,DateOfActivity,Note,Deleted) VALUES (@TypeOfActivity,@TypeOfFruit,@DateOfActivity,@Note,@Deleted)";
                    command.Parameters.Add(new SqlParameter("TypeOfActivity", task.TypeOfActivity.ToString()));
                    command.Parameters.Add(new SqlParameter("TypeOfFruit", task.TypeOfFruit.ToString()));
                    command.Parameters.Add(new SqlParameter("DateOfActivity", Convert.ToDateTime(task.DateOfActivity)));
                    command.Parameters.Add(new SqlParameter("Note", task.Note));
                    command.Parameters.Add(new SqlParameter("Deleted", false));
                    command.ExecuteNonQuery();
                }
            }
        }
        public IEnumerable<Task> GetAllTasks()
        {
            var tasks = new List<Task>();
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                string sql = "SELECT * FROM Task WHERE Deleted = 0";
                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Task task = new Task();
                            task.Id = Convert.ToInt32(rdr["Id"]);
                            task.TypeOfActivity = (ETypeOfActivity)Enum.Parse(typeof(ETypeOfActivity), rdr["TypeOfActivity"].ToString());
                            task.TypeOfFruit = Convert.ToInt32(rdr["TypeOfFruit"].ToString());
                            task.DateOfActivity = DateTime.Parse(rdr["DateOfActivity"].ToString());
                            task.Note = rdr["Note"].ToString();
                            task.Deleted = Convert.ToBoolean(rdr["Deleted"]);
                            tasks.Add(task);
                        }
                    }
                }
            }
            return tasks;
        }
        public void UpdateTask(Task task)
        {
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = @"UPDATE Task SET TypeOfActivity=@TypeOfActivity,TypeOfFruit=@TypeOfFruit,DateOfActivity=@DateOfActivity,Note=@Note,Deleted=@Deleted WHERE Id=@Id";
                    command.Parameters.Add(new SqlParameter("@Id", task.Id));
                    command.Parameters.Add(new SqlParameter("@TypeOfActivity", task.TypeOfActivity.ToString()));
                    command.Parameters.Add(new SqlParameter("@TypeOfFruit", task.TypeOfFruit.ToString()));
                    command.Parameters.Add(new SqlParameter("@DateOfActivity", Convert.ToDateTime(task.DateOfActivity)));
                    command.Parameters.Add(new SqlParameter("@Note", task.Note));
                    command.Parameters.Add(new SqlParameter("@Deleted", false));
                    command.ExecuteNonQuery();
                }
            }
        }
        public bool DeleteTask(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = @"UPDATE Task SET Deleted = 1 WHERE Id=@Id";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    command.ExecuteNonQuery();
                }
            }
            return true;
        }
        public IEnumerable<Task> GetUpcomingTasks()
        {
            var tasks = new List<Task>();
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                string sql = "SELECT TOP 5 * FROM Task WHERE Deleted = 0 AND DateOfActivity > @Date ORDER BY ABS(DATEDIFF(day, @Date, DateOfActivity))";
                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    command.Parameters.Add(new SqlParameter("@Date", DateTime.Now));
                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Task task = new Task();
                            task.Id = Convert.ToInt32(rdr["Id"]);
                            task.TypeOfActivity = (ETypeOfActivity)Enum.Parse(typeof(ETypeOfActivity), rdr["TypeOfActivity"].ToString());
                            task.TypeOfFruit = Convert.ToInt32(rdr["TypeOfFruit"].ToString());
                            task.DateOfActivity = DateTime.Parse(rdr["DateOfActivity"].ToString());
                            task.Note = rdr["Note"].ToString();
                            task.Deleted = Convert.ToBoolean(rdr["Deleted"]);
                            tasks.Add(task);
                        }
                    }
                }
            }
            return tasks;
        }
    }
}
