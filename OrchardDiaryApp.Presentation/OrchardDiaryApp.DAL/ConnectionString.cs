﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace OrchardDiaryApp.DAL
{
    public class ConnectionString
    {
        public string Connection = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
    }
}
