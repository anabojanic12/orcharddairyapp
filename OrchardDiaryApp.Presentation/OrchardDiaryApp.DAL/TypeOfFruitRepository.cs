﻿using OrchardDiaryApp.DAL.Contracts;
using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace OrchardDiaryApp.DAL
{
    public class TypeOfFruitRepository : ITypeOfFruitRepository
    {
        private string _connection;
        public TypeOfFruitRepository(string connectionString)
        {
            this._connection = connectionString;
        }
        public void AddTypeOfFruit(TypeOfFruit typeOfFruit)
        {
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO Fruit(Type) VALUES (@Type)";
                    command.Parameters.Add(new SqlParameter("Type", typeOfFruit.Type));
                    command.ExecuteNonQuery();
                }
            }
        }
        public IEnumerable<TypeOfFruit> GetAll()
        {
            var fruit = new List<TypeOfFruit>();
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                string sql = "SELECT * FROM Fruit";
                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            TypeOfFruit typeOfFruit = new TypeOfFruit();
                            typeOfFruit.Id = Convert.ToInt32(rdr["Id"]);
                            typeOfFruit.Type = rdr["Type"].ToString();
                            fruit.Add(typeOfFruit);
                        }
                    }
                }
            }
            return fruit;
        }
    }
}
