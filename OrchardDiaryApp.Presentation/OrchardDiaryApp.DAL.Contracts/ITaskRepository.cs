﻿using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.DAL.Contracts
{
    public interface ITaskRepository
    {
        IEnumerable<Task> GetAllTasks();
        IEnumerable<Task> GetUpcomingTasks();
        void AddTask(Task task);
        void UpdateTask(Task task);
        bool DeleteTask(int id);
    }
}
