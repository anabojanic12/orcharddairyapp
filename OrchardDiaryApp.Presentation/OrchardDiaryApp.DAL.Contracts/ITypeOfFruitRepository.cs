﻿using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.DAL.Contracts
{
    public interface ITypeOfFruitRepository
    {
        IEnumerable<TypeOfFruit> GetAll();
        void AddTypeOfFruit(TypeOfFruit typeOfFruit);
    }
}
