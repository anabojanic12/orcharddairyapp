﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL.Contracts
{
    public class TypeOfFruitDTO
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
