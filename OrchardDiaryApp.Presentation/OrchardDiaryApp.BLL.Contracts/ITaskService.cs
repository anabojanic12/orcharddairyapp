﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL.Contracts
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetAllTasks();
        IEnumerable<TaskDTO> GetUpcomingTasks();
        void AddTask(TaskDTO taskDTO);
        void UpdateTask(TaskDTO taskDTO);
        bool DeleteTask(int id);
    }
}
