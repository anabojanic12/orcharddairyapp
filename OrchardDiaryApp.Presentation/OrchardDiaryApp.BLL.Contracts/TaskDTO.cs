﻿using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL.Contracts
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int TypeOfFruit { get; set; }
        public ETypeOfActivity TypeOfActivity { get; set; }
        public DateTime DateOfActivity { get; set; }
        public string Note { get; set; }
        public bool Deleted { get; set; }


    }
}
