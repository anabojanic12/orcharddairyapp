﻿using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrchardDiaryApp.BLL.Contracts
{
    public static class DTOConvertorExtension
    {
        public static TaskDTO ToDTO(this Task obj)
        {
            if (obj != null)
            {
                return new TaskDTO()
                {
                    Id = obj.Id,
                    TypeOfFruit = obj.TypeOfFruit,
                    TypeOfActivity = obj.TypeOfActivity,
                    DateOfActivity = obj.DateOfActivity,
                    Note = obj.Note,
                    Deleted = obj.Deleted
                };
            }
            return null;
        }

        public static Task ToObject(this TaskDTO obj)
        {
            if (obj != null)
            {
                return new Task()
                {
                    Id = obj.Id,
                    TypeOfFruit = obj.TypeOfFruit,
                    TypeOfActivity = obj.TypeOfActivity,
                    DateOfActivity = obj.DateOfActivity,
                    Note = obj.Note,
                    Deleted = obj.Deleted
                };
            }
            return null;
        }

        public static TypeOfFruitDTO ToDTO(this TypeOfFruit obj)
        {
            if (obj != null)
            {
                return new TypeOfFruitDTO()
                {
                    Id = obj.Id,
                    Type = obj.Type
                };
            }
            return null;
        }

        public static TypeOfFruit ToObject(this TypeOfFruitDTO obj)
        {
            if (obj != null)
            {
                return new TypeOfFruit()
                {
                    Id = obj.Id,
                    Type = obj.Type
                };
            }
            return null;
        }

        public static IEnumerable<TaskDTO> ToDTOs(this IEnumerable<Task> objects)
        {
            IEnumerable<TaskDTO> objectsDTO = objects.ToList().Select(x => x.ToDTO());
            return objectsDTO;
        }

        public static IEnumerable<Task> ToObjects(this IEnumerable<TaskDTO> objectsDTO)
        {
            IEnumerable<Task> objects = objectsDTO.ToList().Select(x => x.ToObject());
            return objects;
        }

        public static IEnumerable<TypeOfFruitDTO> ToDTOs(this IEnumerable<TypeOfFruit> objects)
        {
            IEnumerable<TypeOfFruitDTO> objectsDTO = objects.ToList().Select(x => x.ToDTO());
            return objectsDTO;
        }

        public static IEnumerable<TypeOfFruit> ToObjects(this IEnumerable<TypeOfFruitDTO> objectsDTO)
        {
            IEnumerable<TypeOfFruit> objects = objectsDTO.ToList().Select(x => x.ToObject());
            return objects;
        }
    }
}
