﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL.Contracts
{
    public interface ITypeOfFruitService
    {
        IEnumerable<TypeOfFruitDTO> GetAll();
        void AddTypeOfFruit(TypeOfFruitDTO typeOfFruitDTO);
    }
}
