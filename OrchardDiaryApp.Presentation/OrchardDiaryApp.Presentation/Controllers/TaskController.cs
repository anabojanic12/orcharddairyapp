﻿using OrchardDiaryApp.BLL;
using OrchardDiaryApp.BLL.Contracts;
using OrchardDiaryApp.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OrchardDiaryApp.Presentation.Controllers
{
    [EnableCors("*","*","*")] 
    public class TaskController : ApiController
    {
        ITaskService taskService = new TaskService(new TaskRepository(new ConnectionString().Connection));

        [HttpGet]
        [Route("api/task")]
        public IHttpActionResult GetAllTasks()
        {
            IEnumerable<TaskDTO> tasks = taskService.GetAllTasks();
            return Ok(tasks);
        }

        [HttpGet]
        [Route("api/task/upcomingtasks")]
        public IHttpActionResult GetUpcomingTasks()
        {
            IEnumerable<TaskDTO> tasks = taskService.GetUpcomingTasks();
            return Ok(tasks);
        }

        [HttpPut]
        [Route("api/task")]
        public IHttpActionResult Update([FromBody] TaskDTO taskDTO)
        {
            taskService.UpdateTask(taskDTO);
            return Ok(); 
        }

        [HttpPost]
        [Route("api/task")]
        public IHttpActionResult Create([FromBody] TaskDTO task)
        {
            taskService.AddTask(task);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, "Successful"));  
        }

        [HttpDelete]
        [Route("api/task/{id}")]
        public IHttpActionResult Delete(int id)
        {
            bool result = taskService.DeleteTask(id);
            return Ok(result);  
        }
    }
}
