﻿using OrchardDiaryApp.BLL;
using OrchardDiaryApp.BLL.Contracts;
using OrchardDiaryApp.DAL;
using OrchardDiaryApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OrchardDiaryApp.Presentation.Controllers
{
    [EnableCors("*", "*", "*")]
    public class TypeOfFruitController : ApiController
    {
        ITypeOfFruitService typeService = new TypeOfFruitService(new TypeOfFruitRepository(new ConnectionString().Connection));

        [HttpGet]
        [Route("api/typeoffruit")]
        public IHttpActionResult Get()
        {
            IEnumerable<TypeOfFruitDTO> typeoffruit = typeService.GetAll();
            return Ok(typeoffruit);
        }

        [HttpPost]
        [Route("api/typeoffruit")]
        public IHttpActionResult AddTypeOfFruit([FromBody] TypeOfFruitDTO typeOfFruit)
        {
            typeService.AddTypeOfFruit(typeOfFruit);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, "Successful"));
        }
    }
}
