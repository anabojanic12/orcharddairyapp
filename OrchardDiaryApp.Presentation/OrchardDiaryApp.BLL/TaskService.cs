﻿using OrchardDiaryApp.BLL.Contracts;
using OrchardDiaryApp.DAL.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;

        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public void AddTask(TaskDTO taskDTO)
        {
            var task = taskDTO.ToObject();
            _taskRepository.AddTask(task); 
        }

        public bool DeleteTask(int id)
        {
            return _taskRepository.DeleteTask(id);
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
           return _taskRepository.GetAllTasks().ToDTOs();
        }

        public IEnumerable<TaskDTO> GetUpcomingTasks()
        {
            return _taskRepository.GetUpcomingTasks().ToDTOs();
        }

        public void UpdateTask(TaskDTO taskDTO)
        {
            var task = taskDTO.ToObject();
            _taskRepository.UpdateTask(task);
        }
    }
}
