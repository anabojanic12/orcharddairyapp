﻿using OrchardDiaryApp.BLL.Contracts;
using OrchardDiaryApp.DAL.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrchardDiaryApp.BLL
{
    public class TypeOfFruitService : ITypeOfFruitService
    {
        private readonly ITypeOfFruitRepository _typeOfFruitRepository;

        public TypeOfFruitService(ITypeOfFruitRepository typeOfFruitRepository)
        {
            this._typeOfFruitRepository = typeOfFruitRepository;
        }
        public void AddTypeOfFruit(TypeOfFruitDTO typeOfFruitDTO)
        {
            var fruit = typeOfFruitDTO.ToObject();
            _typeOfFruitRepository.AddTypeOfFruit(fruit); 
        }

        public IEnumerable<TypeOfFruitDTO> GetAll()
        {
            return _typeOfFruitRepository.GetAll().ToDTOs();
        }
    }
}
